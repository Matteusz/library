package model;

public class Library {
    private static final int MAX_PUBLICATIONS = 2000;
    private int publicationsNumber;
    private Publication[] publications = new Publication[MAX_PUBLICATIONS];


    public void addBook(bookInfo bookInfo) {
        if (publicationsNumber < MAX_PUBLICATIONS) {
            publications[publicationsNumber] = bookInfo;
            publicationsNumber++;
        } else {
            System.out.println("Maksymalna liczba książek.");
        }
    }

    public void printBooks() {
        int countBooks = 0;
        for (int i = 0; i < publicationsNumber; i++) {
            if (publications[i] instanceof bookInfo) {
                System.out.println(publications[i]);
                countBooks++;
            }
        }
        if (countBooks == 0) {
            System.out.println("Brak Książek.");
        }
    }

    public void addMagazine(Magazine magazinee) {
        if (publicationsNumber < MAX_PUBLICATIONS) {
            publications[publicationsNumber] = magazinee;
            publicationsNumber++;
        } else {
            System.out.println("Maksymalna liczba magazynów.");
        }
    }

    public void printMagazine() {
        int countMagazines = 0;
        for (int i = 0; i < publicationsNumber; i++) {
            if (publications[i] instanceof Magazine) {
                System.out.println(publications[i]);
                countMagazines++;
            }
        }
        if (countMagazines == 0) {
            System.out.println("Brak magazynów.");
        }
    }
}



