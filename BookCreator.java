package IO;

import model.Magazine;
import model.bookInfo;

import java.util.Scanner;

public class BookCreator {
    Scanner create = new Scanner(System.in);


    public bookInfo ReadAndCreate() {
        System.out.println("Podaj nazwę książki: ");
        String title = create.nextLine();
        System.out.println("Autor: ");
        String author = create.nextLine();
        System.out.println("Wydawca: ");
        String publisher = create.nextLine();
        System.out.println("ISBN: ");
        String isbn = create.nextLine();
        System.out.println("Data wydania: ");
        int releaseDate = getInt();
        System.out.println("Liczba stron: ");
        int pages = getInt();

        return new bookInfo(title, author, releaseDate, publisher, pages, isbn);
    }

    public Magazine ReadAndCreateMagazine() {
        System.out.println("Podaj Tytuł: ");
        String title = create.nextLine();
        ;
        System.out.println("Wydawca: ");
        String publisher = create.nextLine();
        System.out.println("Język: ");
        String language = create.nextLine();
        System.out.println("Rok wydania: ");
        int year = getInt();
        System.out.println("Miesiąc: ");
        int month = getInt();
        System.out.println("Dzień: ");
        int day = getInt();

        return new Magazine(title, publisher, language, year, month, day);
    }

    public int getInt() {
        int number = create.nextInt();
        create.nextLine();
        return number;
    }

    public void close() {
        create.close();
    }
}


