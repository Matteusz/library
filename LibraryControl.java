package app;

import IO.BookCreator;
import model.Library;
import model.Magazine;
import model.bookInfo;

public class LibraryControl {
    private static final int EXIT = 0;
    private static final int ADD_BOOK = 1;
    private static final int ADD_MAGAZINE = 2;
    private static final int PRINT_BOOKS = 3;
    private static final int PRINT_MAGAZINE = 4;
    private BookCreator bookCreator = new BookCreator();
    private Library library = new Library();

    public void controlLoop() {
        int option;

        do {
            printOptions();
            option = bookCreator.getInt();
            switch (option) {
                case ADD_BOOK:
                    addBook();
                    break;
                case ADD_MAGAZINE:
                    addMagazine();
                    break;
                case PRINT_BOOKS:
                    printBooks();
                    break;
                case PRINT_MAGAZINE:
                    printMagazine();
                    break;
                case EXIT:
                    exit();
                    break;
                default:
                    System.out.println("Błędna opcja.");
            }
        } while (option != EXIT);
    }


    private void printOptions() {
        System.out.println("Wybierz opcje.");
        System.out.println(EXIT + " - wyjście z programu");
        System.out.println(ADD_BOOK + " - dodanie nowej książki");
        System.out.println(ADD_MAGAZINE + " - dodanie nowego magazynu");
        System.out.println(PRINT_BOOKS + " - Wyświetl dostępne książki.");
        System.out.println(PRINT_MAGAZINE + " - Wyświetl dostępny magazyn");

    }

    private void addBook() {
        bookInfo bookInfo = bookCreator.ReadAndCreate();
        library.addBook(bookInfo);
    }

    private void addMagazine() {
        Magazine magazine = bookCreator.ReadAndCreateMagazine();
        library.addMagazine(magazine);
    }

    private void printBooks() {
        library.printBooks();
    }

    private void printMagazine() {
        library.printMagazine();
    }

    private void exit() {
        System.out.println("Koniec programu.");
        bookCreator.close();
    }

}
